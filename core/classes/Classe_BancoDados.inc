<?php
/**
 * Classe básica para acesso a banco de dados
 * @abstract
 */
  abstract class BancoDados {
     public $_default = array(); 
	 public $_db = array();
	 
     abstract public function Conectar();
	 abstract public function Command($params = array(), $_command, $data);	 
  }
 
 class _mongoDb extends BancoDados {
 /**
  * classe para gerenciamento do banco de dados MONGODB
  **/  
 
 public function __construct() {
	$this->_default['_tipo']= 'mongoDb';
 }
 
 public function Conectar() {
	$this->_default['_con'] = new MongoClient();
	$this->_default['_banco'] = $this->_default['_con']->teste;
	$this->_default['_tabela'] = $this->_default['_banco']->users;
	return $this->_default;
 }

 public function Command($params = array(), $_command, $data) {
	if($this->_default['_con'] !== FALSE):
       if ($_command == 'insert'):
			$params->insert($data);
	   Endif;
	   if ($_command == 'find'):
	        $databack = $params->find();
			return $databack;
		Endif;
	Endif; 
 }
}
 ?>
