<?php
	
	define('APP_DIR', 'app');
	define('CORE_DIR', 'core');
	define('CLASSE_DIR', 'classes');
	define('DS', DIRECTORY_SEPARATOR);
	define('ROOT', dirname(__FILE__));
	define('WEBROOT_DIR', 'webroot');
	//	define('WWW_ROOT', ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS);
	if (!defined('FRAMEWORK_CORE')):
		define('FRAMEWORK_CORE', ROOT . DS . 'core');
	Endif;

    require CORE_DIR . DS . CLASSE_DIR . DS . 'index.php';